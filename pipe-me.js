const process = require('process');

function doThatTwerk(fileContents) {
  const myObj = JSON.parse(fileContents);
  console.log(myObj);
  // ... upload to mongo yay whatever
}

// this shit example doesn't deal with all kinds of things correctly though
// including binary data and some other considerations about how to store the
// chunks as they come or what you want to do with them
// better options include
// * concat with a few library choices
// * `pipe` to another writeable stream you have
// * be in a situation like an HTTP endpoint where this matters
let stdinContents = '';
process.stdin.on('data', (chunk) => {
  if (!chunk) {
    return;
  }

  console.log(`debug: reading ${chunk.length} bytes`);
  stdinContents += chunk.toString();
});

process.stdin.on('end', () => {
  doThatTwerk(stdinContents); // "Am I misreading that function" haha
});

process.stdin.on('error', (err) => {
  console.log('Daddy-Reverend why have you abaonded us?', err);
});
